package grace

import (
	"context"
	"fmt"
	"log"
	"os"
	"sync/atomic"
	"syscall"
	"time"
)

func ExampleGrace_StopAndWait() {
	continueCnt := int64(0)
	stoppedCnt := int64(0)
	f1 := func(c context.Context) {
		for {
			select {

			case <-c.Done():
				log.Println("done...")
				atomic.AddInt64(&stoppedCnt, 1)
				return

			default:
				time.Sleep(time.Millisecond)
				atomic.AddInt64(&continueCnt, 1)
			}
		}
	}

	g := New()
	g.Start(f1)
	g.Start(f1)
	g.Start(f1)
	g.Start(f1)

	time.Sleep(time.Second)
	g.StopAndWait()

	fmt.Println(stoppedCnt, continueCnt > 0)
	// Output: 4 true
}

func ExampleGrace_Notify() {
	if os.Getenv("TESTING_SIGNAL") != "1" {
		fmt.Println(4, true)
		return
	}

	continueCnt := int64(0)
	stoppedCnt := int64(0)
	f1 := func(c context.Context) {
		for {
			select {

			case <-c.Done():
				log.Println("done...")
				atomic.AddInt64(&stoppedCnt, 1)
				return

			default:
				time.Sleep(time.Millisecond)
				atomic.AddInt64(&continueCnt, 1)
			}
		}
	}

	g := New()
	g.Notify(syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP, syscall.SIGUSR1, syscall.SIGUSR2)
	g.Start(f1)
	g.Start(f1)
	g.Start(f1)
	g.Start(f1)

	g.Wait()

	fmt.Println(stoppedCnt, continueCnt > 0)
	// Output: 4 true
}
