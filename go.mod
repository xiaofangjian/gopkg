module gitee.com/xiaofangjian/gopkg

go 1.18

require (
	github.com/coocood/freecache v1.2.2
	github.com/go-redis/redis/v8 v8.11.5
	github.com/go-redis/redismock/v8 v8.0.6
	github.com/karlseguin/ccache/v2 v2.0.8
	github.com/vmihailenco/msgpack/v5 v5.3.5
	golang.org/x/sync v0.0.0-20220819030929-7fc1605a5dde
)

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
)
