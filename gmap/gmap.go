package gmap

import (
	"sync"
)

type Map[K comparable, V any] struct {
	m    map[K]V
	lock sync.RWMutex
}

// Put puts a value into the map.
func (m *Map[K, V]) Put(key K, value V) {
	m.lock.Lock()
	defer m.lock.Unlock()

	if m.m == nil {
		m.m = make(map[K]V)
	}
	m.m[key] = value
}

// Get retrieves a value from the map.
func (m *Map[K, V]) Get(key K) (value V, ok bool) {
	m.lock.RLock()
	defer m.lock.RUnlock()

	value, ok = m.m[key]
	return
}

func (m *Map[K, V]) Has(key K) (ok bool) {
	m.lock.RLock()
	defer m.lock.RUnlock()

	_, ok = m.m[key]
	return
}

// Delete deletes a value from the map.
func (m *Map[K, V]) Delete(key K) {
	m.lock.Lock()
	defer m.lock.Unlock()

	delete(m.m, key)
}

// Len returns the number of values in the map.
func (m *Map[K, V]) Len() int {
	m.lock.RLock()
	defer m.lock.RUnlock()

	return len(m.m)
}

// Range calls f sequentially for each key and value present in the map.
// If f returns false, range stops the iteration.
func (m *Map[K, V]) Range(f func(key K, value V) bool) {
	m.lock.RLock()
	defer m.lock.RUnlock()

	for key, value := range m.m {
		if !f(key, value) {
			break
		}
	}
}
