package gpool

import (
	"sync"
)

type Pool[V any] struct {
	sync.Pool
	OnPut func(V)
}

func New[V any](newFunc func() any, onPut func(V)) *Pool[V] {
	return &Pool[V]{
		Pool:  sync.Pool{New: newFunc},
		OnPut: onPut,
	}
}

func (p *Pool[V]) New() V {
	return p.Pool.New().(V)
}

func (p *Pool[V]) Put(x V) {
	p.OnPut(x)
	p.Pool.Put(x)
}
