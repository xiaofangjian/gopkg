package gredis

import (
	"context"
	"sync"

	"github.com/redis/go-redis/v9"
	"gitlab.com/xiaofangjian/gopkg/gpool"
)

type D = map[string]any

var (
	ArgAdd = gpool.New(
		func() any { return &redis.XAddArgs{} },
		func(v *redis.XAddArgs) {
			*v = redis.XAddArgs{}
		})
)

type SimpleQueue struct {
	Client *redis.Client
	Stream string

	sync.Mutex
	buffers []D
}

func (s *SimpleQueue) PushMap(c context.Context, dict D) (msgID string, err error) {
	argv := &redis.XAddArgs{Stream: s.Stream, Values: dict}
	return s.Client.XAdd(c, argv).Result()
}

func (s *SimpleQueue) PopOne(c context.Context) (dict D, err error) {
	argv := &redis.XReadArgs{Streams: []string{s.Stream}}
	xs, err := s.Client.XRead(c, argv).Result()
	if err != nil {
		return
	}

	r := xs[0]
	return r.Messages[0].Values, nil
}

func (s *SimpleQueue) OnExit() (err error) {
	return
}
