package gredis

import (
	"context"
	"fmt"

	"github.com/redis/go-redis/v9"
	"gitlab.com/xiaofangjian/gopkg/codec"
)

func ExampleSimpleQueue() {
	type Item struct {
		Id    int
		Title string
	}

	c := context.Background()
	rc := redis.NewClient(&redis.Options{Addr: "192.168.10.24:6379"})

	s := SimpleQueue[Item]{
		Client: rc,
		Enc:    codec.JsonCodec[Item](),
		Stream: "my-test-stream-queue",
	}
	// id, err := s.PushMap(c, Item{3, "t3"})
	id, err := s.PushMap(c, map[string]any{"id": 3, "title": "t3"})
	fmt.Println(id, err)
	// Output: err
}
