package queue

import (
	"context"
	"math/rand"
	"strconv"
	"testing"

	"github.com/alicebob/miniredis/v2"
	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
)

func TestRedisTopic(t *testing.T) {
	mr := miniredis.RunT(t)
	rds := redis.NewClient(&redis.Options{Addr: mr.Addr()})

	c := context.TODO()

	topic := RedisTopic{
		Rds:   rds,
		Topic: "q1",
	}
	consumer := topic.NewConsumerGroup(`cg1`)

	// prepare data
	assert.Nil(t, rds.XGroupCreateMkStream(c, topic.Topic, consumer.Group, "0").Err())

	// push
	random := strconv.Itoa(rand.Int())
	msgId1, err := topic.PushKV(c, "id", random, "name", "tom")
	assert.Nil(t, err)

	// read
	msgId2, result, err := consumer.ReadOneBlock(c)
	assert.Nil(t, err)
	assert.Equal(t, msgId1, msgId2)
	assert.Equal(t, result["id"], random)

	// read pending
	pendingExts, err := consumer.ReadPending(c, 1, 0)
	assert.Nil(t, err)
	assert.Len(t, pendingExts, 1)

	pendingExt := pendingExts[0]
	assert.Equal(t, pendingExt.ID, msgId1)

	// ack
	ack, err := consumer.Ack(c, pendingExt.ID)
	assert.Nil(t, err)
	assert.Equal(t, ack, int64(1))

}
