package queue

import (
	"context"
	"os"
	"time"

	"github.com/redis/go-redis/v9"
)

type RedisTopic struct {
	Rds   *redis.Client
	Topic string
}

func (rt *RedisTopic) PushKV(c context.Context, keyAndValues ...string) (msgId string, err error) {
	msgId, err = rt.Rds.XAdd(c, &redis.XAddArgs{
		Stream: rt.Topic,
		Values: keyAndValues,
	}).Result()
	return
}

func (rt *RedisTopic) NewConsumerGroup(group string) *RedisConsumer {
	return &RedisConsumer{
		Topic: rt,
		Group: group,
	}
}

type RedisConsumer struct {
	Topic *RedisTopic
	Group string
}

func (rc *RedisConsumer) ReadOneBlock(c context.Context) (msgId string, result map[string]any, err error) {
	consumerName, _ := os.Hostname()

	xStreams, err := rc.Topic.Rds.XReadGroup(c, &redis.XReadGroupArgs{
		Group:    rc.Group,
		Consumer: consumerName,
		Streams:  []string{rc.Topic.Topic, ">"},
		Count:    1,
		Block:    time.Second * 30,
	}).Result()
	if err != nil {
		return
	}

	xmsgs := xStreams[0].Messages
	if len(xmsgs) == 0 {
		return
	}

	msgId = xmsgs[0].ID
	result = xmsgs[0].Values
	return
}

func (rc *RedisConsumer) Ack(c context.Context, msgIds ...string) (cnt int64, err error) {
	return rc.Topic.Rds.XAck(c, rc.Topic.Topic, rc.Group, msgIds...).Result()
}

func (rc *RedisConsumer) ReadPending(c context.Context, n int, tll time.Duration) (pendingExts []redis.XPendingExt, err error) {
	xPend, err := rc.Topic.Rds.XPendingExt(c, &redis.XPendingExtArgs{
		Stream: rc.Topic.Topic,
		Group:  rc.Group,
		Start:  "-",
		End:    "+",
		Count:  int64(n),
		Idle:   tll,
	}).Result()
	if err != nil {
		return
	}

	return xPend, nil
}
