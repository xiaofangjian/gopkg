package gslice

import (
	"fmt"
	"strconv"
)

func ExampleMap() {
	s := []int{1, 3, 2, 4, 5}
	fmt.Println(Map(s, func(i int) string { return strconv.Itoa(i*10 + i) }))
	// Output: [11 33 22 44 55]
}

func ExampleKeyBy() {
	s := []int{1, 3, 2, 4, 5}
	fmt.Println(KeyBy(s, func(i int) string { return strconv.Itoa(i*10 + i) }))
	// Output: map[11:1 22:2 33:3 44:4 55:5]
}

func ExampleOnly() {
	s := []int{1, 3, 2, 4, 5}
	fmt.Println(Only(s, func(i int) bool { return i&1 == 1 }))
	// Output: [1 3 5]
}
