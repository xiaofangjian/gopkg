package gslice

func Map[T1, T2 any](s []T1, f func(T1) T2) (out []T2) {
	for _, e := range s {
		out = append(out, f(e))
	}
	return
}

func KeyBy[T1 any, T2 comparable](s []T1, f func(e T1) T2) (out map[T2]T1) {
	out = make(map[T2]T1, len(s))
	for _, e := range s {
		out[f(e)] = e
	}
	return
}

func Only[T any](s []T, f func(T) bool) (out []T) {
	for _, e := range s {
		if f(e) {
			out = append(out, e)
		}
	}
	return
}

func Filter[T any](s []T, f func(T) bool) (out []T) {
	return Only(s, func(e T) bool { return f(e) })
}
