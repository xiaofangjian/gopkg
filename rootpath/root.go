package rootpath

import (
	"os"
	"strings"
)

// func UpTo(folder string) {
// 	_ = os.Chdir(FindRootPath(folder))
// 	wd, _ := os.Getwd()
// 	log.Println("PWD=", wd)
// }

func FindRootPath(root string) string {
	dir, _ := os.Getwd()
	dirs := strings.Split(dir, "/")
	for i := len(dirs) - 1; i >= 0; i-- {
		if dirs[i] == root {
			rootPath := strings.Join(dirs[:i+1], "/")
			return rootPath
		}
	}
	return ""
}
