package cachee

import (
	"context"
	"fmt"
	"time"

	"github.com/coocood/freecache"
	"github.com/go-redis/redismock/v8"
)

func ExampleFreeCacheKvStore() {
	store := NewFreeCacheKvStore(freecache.NewCache(32), MsgPackEncoder[string]())
	ctx := context.TODO()

	v, exist, err := store.Get(ctx, `name`)
	fmt.Println(v == ``, exist, err)

	err = store.Set(ctx, `name`, `alice`, time.Second)
	fmt.Println(err)

	v, exist, err = store.Get(ctx, `name`)
	fmt.Println(v, exist, err)

	time.Sleep(time.Second)
	v, exist, err = store.Get(ctx, `name`)
	fmt.Println(v == ``, exist, err)

	// Output:
	// true false <nil>
	// <nil>
	// alice true <nil>
	// true false <nil>
}

func ExampleRedisKvStore() {
	db, mock := redismock.NewClientMock()

	store := NewRedisKvStore(db, JsonEncoder[string]())
	ctx := context.TODO()

	mock.ExpectGet(`name`).RedisNil()
	v, exist, err := store.Get(ctx, `name`)
	fmt.Println(v == ``, exist, err)

	cmd1 := mock.ExpectSet(`name`, []byte(`"alice"`), time.Second)
	cmd1.SetVal("")
	cmd1.SetErr(nil)
	err = store.Set(ctx, `name`, `alice`, time.Second)
	fmt.Println(err)

	cmd2 := mock.ExpectGet(`name`)
	cmd2.SetVal(`"alice"`)
	cmd2.SetErr(nil)
	v, exist, err = store.Get(ctx, `name`)
	fmt.Println(v, exist, err)

	time.Sleep(time.Second)
	mock.ExpectGet(`name`).RedisNil()
	v, exist, err = store.Get(ctx, `name`)
	fmt.Println(v == ``, exist, err)

	// Output:
	// true false <nil>
	// <nil>
	// alice true <nil>
	// true false <nil>
}
