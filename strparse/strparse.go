package strparse

import (
	"strings"
)

// Last split s by sep, and return the last elements.
// if s don't contains sep, return s
// if s == sep, return ""
func Last(s string, sep string) string {
	if sep == "" {
		return s
	}
	idx := strings.LastIndex(s, sep)
	if idx == -1 {
		return s
	} else if idx == len(s)-len(sep) {
		return ""
	} else {
		return s[idx+len(sep):]
	}
}
