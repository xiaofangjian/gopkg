package codec

import (
	"encoding/json"
)

type EncodeDecoder[V any] struct {
	Encode func(V) ([]byte, error)
	Decode func(data []byte) (V, error)
}

func JsonCodec[V any]() EncodeDecoder[V] {
	return EncodeDecoder[V]{
		Encode: func(v V) ([]byte, error) { return json.Marshal(v) },
		Decode: func(data []byte) (v V, err error) { err = json.Unmarshal(data, &v); return },
	}
}
