package oo

type R[V any] struct {
	v   V
	err error
}

func ResultOf[V any](v V, err error) *R[V] {
	return &R[V]{v: v, err: err}
}

func ErrOf(err error) *R[any] {
	return &R[any]{err: err}
}

func (r *R[V]) Must() V {
	if r.err != nil {
		panic(r.err)
	}
	return r.v
}

func (r *R[V]) V() V {
	return r.v
}
