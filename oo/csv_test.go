package oo

import (
	"encoding/csv"
	"fmt"
	"strings"
)

func ExampleCSVRead() {
	s := "name,age\nalice,18\nbob,20"
	rows, err := CSVRead(csv.NewReader(strings.NewReader(s)))

	fmt.Println(rows[0]["name"], rows[1]["age"], err)
	// Output: alice 20 <nil>
}
